# Information / Информация

Добавление **emoji** в статью.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Emoji`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Emoji' );
```

## Syntax / Синтаксис

```html
{{#emoji: [ID]|[SIZE]}}
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
